Contributors
============
This file contains a list of every person who has contributed code to Overviewer. It should include everyone, but we may have missed a few and it is manually updated now. If you feel like you've been left out, feel free to tell us!

Not currently included (but hopefully soon) are testers and bug reporters that help to fix the many bugs that have popped up in the course of development.

Original Authors
----------------
### Owners

* Patrick Kleinschmidt (NoxNebula) \<nox.nebula@gmx.de\>

### Collaborators

* Daniel Erpelding (Cadillaxx) \<thecadillaxx@gmail.com\>
* Yannick Stephan (Nut) \<info@yannickweb.de\>
* Gillo Braun (Angus) \<braun.gillo@hotmail.com\>

Long-term Contributions
-----------------------
These contributors have made many changes, over a fairly long time span, or for many different parts of the code.

Short-term Contributions
------------------------
These contributors have made specific changes for a particular bug fix or feature.

* Janek Ostendorf (ozzy) \<ozzy2345de@gmail.com\>
* Yared Hufkens (Malachite) \<yared@malachite.de.vu\>