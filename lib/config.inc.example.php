<?php
define('CFG', 1);
define('CFG_DEBUG', 1);

// Database Config
define('CFG_DB_TYPE',     'MySQL');
define('CFG_DB_USER',     'root');
define('CFG_DB_PASSWORD', '');
define('CFG_DB_DATABASE', 'silexboard');
define('CFG_DB_PREFIX',   '');
define('CFG_DB_HOST',     'localhost');
define('CFG_DB_PORT',     '');
define('CFG_DB_SOCKET',   '');

define('CFG_CACHE_DIR',   DIR_ROOT.'lib/cache/');

// URL info
//define('CFG_BASE_URL', 'http://board/');
define('CFG_BASE_URL', '/');

/* --- Currently not needed stuff --- */

// Cache info
define('CFG_CACHE_TYPE',  'File');
