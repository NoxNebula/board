<?php
/**
 * Printable exceptions have their own outputk
 *
 * @author     SilexBB
 * @copyright  2011 - 2012 Silex Bulletin Board
 * @license    GPL version 3 <http://www.gnu.org/licenses/gpl-3.0.html>
 */

interface IPrintableException {
	public function Show();
}
