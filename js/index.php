<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<title>javascript test</title>
	<script type="text/javascript" src="jquery.min.js"></script>
	<script type="text/javascript" src="jquery.animate-colors-min.js"></script>
	<script type="text/javascript" src="jquery.animate-shadow-min.js"></script>
	<script type="text/javascript" src="jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="test.js"></script>
	<style type="text/css">
		#content {
			display: block;
			height: 250px;
			width: 300px;
			background: #333333;
			color: #ffffff;
		}
	</style>
</head>
<body>
	<div id="content">
		Content-DIV, welcher animiert werden soll.
	</div>
	<button id="animate">Show / Hide</button>
</body>
</html>